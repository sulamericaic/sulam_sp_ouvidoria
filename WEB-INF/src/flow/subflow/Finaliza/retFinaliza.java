package flow.subflow.Finaliza;

/**
 * This servlet is the exit point of a subflow.  The base class handles
 * the logic for forwarding to the next servlet.
 * Last generated by Orchestration Designer at: 13 DE SETEMBRO DE 2018 10H13MIN43S BRT
 */
public class retFinaliza extends com.avaya.sce.runtime.SubflowReturn {

	//{{START:CLASS:FIELDS
	//}}END:CLASS:FIELDS

	/**
	 * Default constructor
	 * Last generated by Orchestration Designer at: 13 DE SETEMBRO DE 2018 10H13MIN43S BRT
	 */
	public retFinaliza() {
		//{{START:CLASS:CONSTRUCTOR
		super();
		//}}END:CLASS:CONSTRUCTOR
	}

}
