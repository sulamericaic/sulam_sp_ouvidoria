package br.com.sulamerica.ura.avaya.utils;

import com.avaya.sce.runtimecommon.ITraceInfo;
import com.avaya.sce.runtimecommon.SCESession;

public class LogUtils {
	/**
	 * 
	 * @param mySession
	 * @param nomeClass
	 */
	public static void logEntradaFlow(SCESession mySession, String nomeClass) {
		ITraceInfo logger = mySession.getTraceOutput();
		
		logger.writeln(2, "##############################################");
		
		nomeClass = nomeClass.replaceAll("entrada", "");
		logger.writeln(2, "##############################################");
		logger.writeln(2, "## "+ nomeClass);
		
		mySession.setProperty("CLASS_NAME_FLOW", nomeClass);
	}
	
	/**
	 * typeMessage Legend
	 * 1 - Info 
	 * 2 - Debug 
	 * 3 - Warm 
	 * 4 - Erro 
	 * @param mySession
	 * @param message
	 * @param typeMessage
	 */
	public static void writeLogFlow(SCESession mySession, String message, int typeMessage) {
		ITraceInfo logger = mySession.getTraceOutput();
		
		String nomeClass = mySession.getProperty("CLASS_NAME_FLOW").toString();
		
		logger.writeln(typeMessage, nomeClass + " : " +message);
	}
	
	public static void logSaidaFlow(SCESession mySession) {
		ITraceInfo logger = mySession.getTraceOutput();
		logger.writeln(2, "##############################################");
		
		mySession.setProperty("CLASS_NAME_FLOW", "");
	}
	
	public static void logEntradaSubFlow(SCESession mySession, String nomeClass) {
		ITraceInfo logger = mySession.getTraceOutput();
		
		logger.writeln(2, "##############################################");
		nomeClass = nomeClass.replaceAll("entrada", "");
		logger.writeln(2, "## "+ nomeClass);
		
	}
}
