package br.com.sulamerica.ura.avaya.utils;

import com.avaya.sce.runtimecommon.SCESession;

import br.com.sulamerica.basedadosoperacoes.Relatorio;
import br.com.sulamerica.bean.BaseUra;
import flow.IProjectVariables;

public class RelatorioUtils {
	public static int retornaApenasPonto(String valor) {
		StringBuffer sb = new StringBuffer();
		 
		for (char a : valor.toCharArray()) {
			if (Character.isDigit(a))
				sb.append(a);
		}
		
		return Integer.parseInt(sb.toString());
	}
	
	public static void insereRelatorio(BaseUra baseUra, SCESession mySession) {
		int timeout = mySession.getVariableField(IProjectVariables.RELATORIO,
				IProjectVariables.RELATORIO_FIELD_TIMEOUT).getIntValue();
		
		try {
			Relatorio.gravaRelatorio(baseUra, timeout);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
