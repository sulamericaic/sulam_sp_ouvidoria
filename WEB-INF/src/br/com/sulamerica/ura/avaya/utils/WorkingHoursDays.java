package br.com.sulamerica.ura.avaya.utils;

import java.util.Calendar;
import java.util.TimeZone;

public class WorkingHoursDays {


	// class/global var
	private final static String Local_TZ_Str_ID = "America/Sao_Paulo";


	// formats
	static Boolean validate_day_prefix(String period)
	{
		//evd = every day
		//wkd = weekdays
		//wnd = weekends
		//mon,tue,wed,thu,fri,sat,sun = a weekday  

		Boolean local_bool;
		String local_str;
		local_bool = false; 
		local_str = period.substring(0,3);

		local_bool = validate_a_weekday_prefix(period);

		if (local_bool == false)
		{
			local_bool = validate_every_weekday_prefix(period);
		}

		if (local_bool == false)
		{
			local_bool = validate_weekend_prefix(period);
		}

		if (local_bool == false)
		{
			local_bool = validate_every_day_prefix(period);
		}

		// special day
		if (local_bool == false)
		{
			local_bool = validate_special_day_prefix(period);
		}
		// special day

		return local_bool;
	}

	static Boolean validate_a_weekday_prefix(String weekday)
	{
		//sun,mon,tue,wed,thu,fri,sat = a weekday

		Boolean local_bool;
		String local_str;
		local_bool = false; 
		local_str = weekday.substring(0,3);


		if ("sun".equals(local_str )) {
			local_bool = true;
		}
		else if ("mon".equals(local_str )) {
			local_bool = true;
		}
		else if ("tue".equals(local_str )) {
			local_bool = true;
		}
		else if ("wed".equals(local_str )) {
			local_bool = true;
		}
		else if ("thu".equals(local_str )) {
			local_bool = true;
		}
		else if ("fri".equals(local_str )) {
			local_bool = true;
		}
		else if ("sat".equals(local_str )) {
			local_bool = true;
		}

		return local_bool;
	}

	static Boolean validate_every_weekday_prefix(String period)
	{
		//wkd = weekdays = every weekday = mon-fri

		Boolean local_bool;
		String local_str;
		local_bool = false; 
		local_str = period.substring(0,3);

		if ("wkd".equals(local_str )) {
			local_bool = true;
		}

		return local_bool;
	}

	// special day
	static Boolean validate_special_day_prefix(String period)
	{
		//spe = special day = any given day used for special days when working times differ from the standard 

		Boolean local_bool;
		String local_str;
		local_bool = false; 
		local_str = period.substring(0,3);

		if ("spe".equals(local_str )) {
			local_bool = true;
		}

		return local_bool;
	}
	// special day

	static Boolean validate_weekend_prefix(String period)
	{
		//wnd = weekends

		Boolean local_bool;
		String local_str;
		local_bool = false; 
		local_str = period.substring(0,3);

		if ("wnd".equals(local_str )) {
			local_bool = true;
		}

		return local_bool;
	}

	static Boolean validate_every_day_prefix(String period)
	{
		//evd = every day

		Boolean local_bool;
		String local_str;
		local_bool = false; 
		local_str = period.substring(0,3);

		if ("evd".equals(local_str )) {
			local_bool = true;
		}

		return local_bool;
	}


	// time
	static Boolean validate_ascii_4_digit_time(String timestr)
	{
		//0800

		Boolean local_bool;
		String local_str, local_digit_1,local_digit_2, local_digit_3,local_digit_4;
		local_bool = false; 
		local_str = timestr.substring(0,4);
		local_digit_1 = local_str.substring(0,1);
		local_digit_2 = local_str.substring(1,2);
		local_digit_3 = local_str.substring(2,3);
		local_digit_4 = local_str.substring(3,4);

		if (
				(validate_ascii_single_digit(local_digit_1) == true) &&
				(validate_ascii_single_digit(local_digit_2) == true) &&
				(validate_ascii_single_digit(local_digit_3) == true) &&
				//(validate_ascii_single_digit_0_1_3_4(local_digit_3) == true) &&
				//(validate_ascii_single_digit_0_5(local_digit_4) == true)
				(validate_ascii_single_digit(local_digit_4) == true)
				)
		{
			local_bool = true;
		}

		return local_bool;
	}

	static Boolean validate_ascii_single_digit(String singledigit)
	{
		//0,1,2,3,4,5,6,7,8,9

		Boolean local_bool;
		String local_str;
		local_bool = false; 
		local_str = singledigit.substring(0,1);

		if (("0".equals(local_str )) || ("1".equals(local_str )) || 
				("2".equals(local_str )) || ("3".equals(local_str )) ||
				("4".equals(local_str )) || ("5".equals(local_str )) ||
				("6".equals(local_str )) || ("7".equals(local_str )) ||
				("8".equals(local_str )) || ("9".equals(local_str )) )
		{
			local_bool = true;
		}

		return local_bool;
	}


	// working hours/intervals
	static Boolean validate_type_1_custom_working_hours_interval(String working_hours_interval_str)
	{
		//mon0800-1800

		Boolean local_bool;
		String local_str;
		String local_day, local_start_time,local_internal_separator, local_stop_time;

		local_bool = false; 


		if (working_hours_interval_str.length()>=12)
		{

			local_str = working_hours_interval_str.substring(0,12);

			local_day = local_str.substring(0,3);
			local_start_time = local_str.substring(3,7);
			local_internal_separator = local_str.substring(7,8);
			local_stop_time = local_str.substring(8,12);


			if (
					(validate_day_prefix(local_day)) &&
					(validate_ascii_4_digit_time(local_start_time))&&
					(validate_ascii_single_separator(local_internal_separator))&&
					(validate_ascii_4_digit_time(local_stop_time)) &&
					(validate_time_order_hhmm(local_start_time, local_stop_time))
					)
			{
				local_bool = true;
			}
		} // end if length >= 12

		return local_bool;
	}

	static Boolean validate_ascii_single_separator(String separator_str)
	{
		// -

		Boolean local_bool;
		String local_str;
		local_bool = false; 
		local_str = separator_str.substring(0,1);

		if ("-".equals(local_str )) 
		{
			local_bool = true;
		}

		return local_bool;
	}


	// dates
	static boolean checkDayofWeek(int dayofweek, String prefix)
	{
		boolean res_match_dayofweek;
		res_match_dayofweek = false;

		try
		{
			if (dayofweek == 1) 
			{
				if ((prefix.equals("sun") == true) || // sundays
						(prefix.equals("wnd") == true) || // weekends
						(prefix.equals("evd") == true) )  // every day
				{	
					res_match_dayofweek = true;

				}
			}

			if (dayofweek == 2) 
			{
				if ((prefix.equals("mon") == true) || // mondays
						(prefix.equals("wkd") == true) || // weekdays
						(prefix.equals("evd") == true) )  // every day
				{	
					res_match_dayofweek = true;
				}
			}

			if (dayofweek == 3) 
			{
				if ((prefix.equals("tue") == true) || // tuesdays
						(prefix.equals("wkd") == true) || // weekdays
						(prefix.equals("evd") == true) )  // every day
				{	
					res_match_dayofweek = true;

				}
			}

			if (dayofweek == 4) 
			{
				if ((prefix.equals("wed") == true) || // wednesdays
						(prefix.equals("wkd") == true) || // weekdays
						(prefix.equals("evd") == true) )  // every day
				{	
					res_match_dayofweek = true;

				}
			}

			if (dayofweek == 5) 
			{
				if ((prefix.equals("thu") == true) || // thursdays
						(prefix.equals("wkd") == true) || // weekdays
						(prefix.equals("evd") == true) )  // every day
				{	
					res_match_dayofweek = true;

				}
			}

			if (dayofweek == 6) 
			{
				if ((prefix.equals("fri") == true) || // fridays
						(prefix.equals("wkd") == true) || // weekdays
						(prefix.equals("evd") == true) )  // every day
				{	
					res_match_dayofweek = true;

				}
			}

			if (dayofweek == 7) 
			{
				if ((prefix.equals("sat") == true) || // saturdays
						(prefix.equals("wnd") == true) || // weekends
						(prefix.equals("evd") == true) )  // every day
				{	
					res_match_dayofweek = true;

				}
			}

			// special day
			if (prefix.equals("spe") == true) 
			{
				res_match_dayofweek = true;
			}
			// special day

		}
		catch (Exception ex)
		{
			res_match_dayofweek = false;
		}

		return res_match_dayofweek;
	}

	static boolean checktime_hhmm(String starttime, String stoptime, String currenttime)
	{
		boolean res_match_time;
		res_match_time = false;
		int  start_time_int, stop_time_int, current_time_int;

		try
		{
			start_time_int = Integer.parseInt(starttime);
			stop_time_int = Integer.parseInt(stoptime);
			current_time_int = Integer.parseInt(currenttime);

			if ((start_time_int <= current_time_int) && (stop_time_int >= current_time_int))
			{
				res_match_time = true;
			}
		}
		catch (Exception ex)
		{
			res_match_time = false;
		}

		return res_match_time;
	}

	static boolean validate_time_order_hhmm(String starttime, String stoptime)
	{
		boolean res_time_order;
		res_time_order = false;
		int  start_time_int, stop_time_int;

		try
		{
			start_time_int = Integer.parseInt(starttime);
			stop_time_int = Integer.parseInt(stoptime);

			if (start_time_int < stop_time_int)
			{
				res_time_order = true;
			}
		}
		catch (Exception ex)
		{
			res_time_order = false;
		}

		return res_time_order;
	}

	static boolean check_time_input(int hourOfDay, int minute)
	{
		boolean res_match_time;
		res_match_time = false;

		if (((hourOfDay >= 0) && (hourOfDay <= 23)) && ((minute >= 0) && (minute <= 60)))	
		{
			res_match_time = true;
		}

		return res_match_time;
	}

	static String Conv_Time_as_hhmm_string(int  hourOfDay, int minute)
	{
		// 0830
		String res_time_str;
		String Hourstr, Minutestr;
		res_time_str = "";
		Hourstr = "";
		Minutestr = "";	

		try
		{	
			if ((hourOfDay >= 0) && (hourOfDay <= 60) &&
					(minute >= 0) && (minute <= 60))
			{   
				Hourstr = String.format("%02d", hourOfDay);
				Minutestr = String.format("%02d", minute);
				res_time_str = Hourstr + Minutestr;
			}
		}
		catch (Exception ex)
		{
			res_time_str = "";
		}

		return res_time_str;
	}

	static boolean check_input(String WorkingHours ,int date_diff_from_today_in_days, int  hourOfDay, int minute)
	{
		boolean res_match_date_time, input_datetime_ok, input_workingHours_ok;
		boolean day_ok;
		boolean time_ok;
		int dayofweek;
		String local_str;
		String local_day, local_start_time,local_internal_separator, local_stop_time;
		String currenttime_str;

		//init vars
		res_match_date_time = false;
		input_datetime_ok = false;
		input_workingHours_ok = false;
		day_ok = false;
		time_ok = false;
		dayofweek = 0;
		local_str = "";
		local_day = "";
		local_start_time = "";
		local_internal_separator = "";
		local_stop_time = "";
		currenttime_str = "";


		try
		{	
			input_datetime_ok = check_time_input(hourOfDay, minute);

			input_workingHours_ok = validate_type_1_custom_working_hours_interval(WorkingHours);


			if (input_datetime_ok && input_workingHours_ok)
			{			
				dayofweek = getDayofWeek(date_diff_from_today_in_days);


				if (WorkingHours.length()>=12)
				{

					local_str = WorkingHours.substring(0,12);

					local_day = local_str.substring(0,3);
					local_start_time = local_str.substring(3,7);
					local_internal_separator = local_str.substring(7,8);
					local_stop_time = local_str.substring(8,12);

					currenttime_str = Conv_Time_as_hhmm_string(hourOfDay, minute);	

					day_ok = checkDayofWeek(dayofweek,local_day);
					time_ok = checktime_hhmm(local_start_time, local_stop_time, currenttime_str);

				} // end if length >= 12
			}


			if (day_ok && time_ok)
			{
				res_match_date_time = true;
			}

		}
		catch (Exception ex)
		{
			res_match_date_time = false;
		}

		return res_match_date_time;
	}

	public static int getDayofWeek(int diff_in_days)
	{
		int res_dayofweek;
		res_dayofweek =0;

		try
		{   
			TimeZone Localzone;
			Localzone = TimeZone.getTimeZone(Local_TZ_Str_ID);

			Calendar c = Calendar.getInstance(Localzone);

			c.setFirstDayOfWeek(1);

			c.add(Calendar.DATE, diff_in_days);

			int day_of_week = c.get(Calendar.DAY_OF_WEEK);

			res_dayofweek = day_of_week;
		}
		catch (Exception ex)
		{
			res_dayofweek = -1;
		}

		return res_dayofweek;
	}


	//publics 	
	public static boolean Check_Working_Hours(String WorkingHours, int diff_from_today_in_days, int Hour, int Minute)
	{
		boolean local_bool_res, temp_bool;
		local_bool_res = false; 
		temp_bool = false;
		String[] StrArray;

		StrArray = WorkingHours.split(";");	

		if (StrArray.length >0 )
		{	
			for(int i = 0; i <= StrArray.length -1; i++ )
			{
				temp_bool = WorkingHoursDays.check_input(StrArray[i],diff_from_today_in_days, Hour, Minute);
				if (temp_bool)
				{
					local_bool_res = true;
				} 
			}	
		}	
		return local_bool_res;	
	}

	//publics 
	public static boolean is_it_now_within_Working_Hours(String WorkingHours)
	{
		boolean local_bool_res;
		int Hour, Minute;
		local_bool_res = false; 

		TimeZone Localzone;
		Localzone = TimeZone.getTimeZone(Local_TZ_Str_ID);

		Calendar cal = Calendar.getInstance(Localzone);
		Hour = cal.get(Calendar.HOUR_OF_DAY);
		Minute = cal.get(Calendar.MINUTE);

		//System.out.print("Hour="+String.valueOf(Hour));
		//System.out.print(" Minute="+String.valueOf(Minute));


		local_bool_res = Check_Working_Hours(WorkingHours, 0,  Hour, Minute);

		return local_bool_res;	
	}

	/////////////////////////////	
	/////////////////////////////
	/////////////////////////////
	/////////////////////////////
	/// feriados

	///////

	//publics 
	public static boolean Check_is_today_listed_as_a_Holiday(String Holidays, int diff_in_days)
	{
		boolean local_bool_res, temp_bool;
		String[] StrArray;

		local_bool_res = false; 
		temp_bool = false;


		StrArray = Holidays.split(";");


		if (StrArray.length >0 )
		{	
			for(int i = 0; i <= StrArray.length -1; i++ )
			{
				temp_bool = is_it_today_a_listed_holiday(StrArray[i], diff_in_days);
				if (temp_bool)
				{
					local_bool_res = true;
				} 

			}	
		}	
		return local_bool_res;	
	}

	//// new code for special days - begin
	//publics 
	public static boolean is_it_now_within_working_hours_of_a_listed_special_day(String list_of_Special_days)
	{
		boolean local_bool_res;
		int Hour, Minute;
		local_bool_res = false; 

		TimeZone Localzone;
		Localzone = TimeZone.getTimeZone(Local_TZ_Str_ID);

		Calendar cal = Calendar.getInstance(Localzone);
		Hour = cal.get(Calendar.HOUR_OF_DAY);
		Minute = cal.get(Calendar.MINUTE);

		local_bool_res = Check_is_this_time_within_working_hours_of_a_listed_special_day(list_of_Special_days,  Hour, Minute);

		return local_bool_res;	
	}

	//publics 
	public static boolean Check_is_this_time_within_working_hours_of_a_listed_special_day(String list_of_Special_days, int Hour, int Minute)
	{
		boolean local_bool_res, temp_bool;
		String[] StrArray;

		local_bool_res = false; 
		temp_bool = false;


		StrArray = list_of_Special_days.split(";");


		if (StrArray.length >0 )
		{	
			for(int i = 0; i <= StrArray.length -1; i++ )
			{
				temp_bool = is_this_time_within_working_hours_of_the_special_day(StrArray[i], Hour, Minute);
				if (temp_bool)
				{
					local_bool_res = true;
				} 

			}	
		}	
		return local_bool_res;	
	}

	static boolean is_this_time_within_working_hours_of_the_special_day(String special_day_str,  int Hour, int Minute)
	{
		// special day example:
		// 07sep16spe0800-1800

		boolean local_bool_res; 
		boolean local_bool_phase_1; 
		boolean local_bool_phase_2;
		int Day, Month, Year, parsedDay, parsedMonth, parsedYear ;
		String Local_special_day;
		String Local_times_for_special_day;
		local_bool_res = false;
		local_bool_phase_1 = false;
		local_bool_phase_2 = false;
		Local_special_day = "";
		Local_times_for_special_day = "";

		int diff_from_today_in_days = 0;

		TimeZone Localzone;
		Localzone = TimeZone.getTimeZone(Local_TZ_Str_ID);

		Calendar cal = Calendar.getInstance(Localzone);

		Day = cal.get(Calendar.DAY_OF_MONTH);
		Month = cal.get(Calendar.MONTH);
		Year =  cal.get(Calendar.YEAR);

		Local_special_day = special_day_str.trim();

		if (Local_special_day.length() == 17)
		{
			parsedDay = get_integer_value_day_of_month_from_string(Local_special_day.substring(0, 2));
			parsedMonth = map_month_prefix_to_zero_based_int(Local_special_day.substring(2, 5));



			if ((Day == parsedDay) && (Month == parsedMonth))
			{
				local_bool_phase_1 = true; 
				Local_times_for_special_day = Local_special_day.substring(5,17);
			}
		}

		if (Local_special_day.length() == 19)
		{
			parsedDay = get_integer_value_day_of_month_from_string(Local_special_day.substring(0, 2));
			parsedMonth = map_month_prefix_to_zero_based_int(Local_special_day.substring(2, 5));
			parsedYear = get_integer_value_from_a_2_digit_number_in_a_string(Local_special_day.substring(5, 7));

			parsedYear = parsedYear + 2000;

			if ((Day == parsedDay) && (Month == parsedMonth) && (Year == parsedYear))
			{
				local_bool_phase_1 = true;
				Local_times_for_special_day = Local_special_day.substring(7,19);
			}
		}

		if (local_bool_phase_1)
		{
			local_bool_phase_2 = WorkingHoursDays.check_input(Local_times_for_special_day, diff_from_today_in_days, Hour, Minute);

			if (local_bool_phase_2)
			{
				local_bool_res = true;
			} 
		} else
		{
			// it lets all other days pass as open - because they will be treated later with other functions - either usual working hours or holidays
			// only the exact special days will get a different time - and will be filter here
			local_bool_res = true;
		}


		return local_bool_res;	
	}



	//// new code for special days - end


	//publics 
	public static int get_integer_value_from_a_2_digit_number_in_a_string(String two_digits_as_string)
	{
		int local_int_res;
		String local_temp_str, Local_2_digit_number_as_string;

		local_int_res = -1; 
		local_temp_str= "";
		Local_2_digit_number_as_string = "";

		local_temp_str = two_digits_as_string.trim();

		if (local_temp_str.length() > 1)
		{
			Local_2_digit_number_as_string = local_temp_str.substring(0, 2) ;

			Local_2_digit_number_as_string = Local_2_digit_number_as_string.trim();
		}

		if (local_temp_str.length() == 1)
		{
			Local_2_digit_number_as_string = local_temp_str ;
		}

		try
		{
			local_int_res = Integer.parseInt(Local_2_digit_number_as_string);
		}
		catch(Exception ex)
		{
			local_int_res = -2;
		}

		return local_int_res;	
	}


	static boolean is_it_today_a_listed_holiday(String holiday_str, int diff_in_days)
	{
		boolean local_bool_res; 
		int Day, Month, Year, parsedDay, parsedMonth, parsedYear ;
		String Local_non_working_day;
		local_bool_res = false; 
		Local_non_working_day = "";

		TimeZone Localzone;
		Localzone = TimeZone.getTimeZone(Local_TZ_Str_ID);

		Calendar cal = Calendar.getInstance(Localzone);
		cal.setFirstDayOfWeek(1);
		cal.add(Calendar.DATE, diff_in_days);

		Day = cal.get(Calendar.DAY_OF_MONTH);
		Month = cal.get(Calendar.MONTH);
		Year =  cal.get(Calendar.YEAR);

		Local_non_working_day = holiday_str.trim();

		if (Local_non_working_day.length() == 5)
		{
			parsedDay = get_integer_value_day_of_month_from_string(Local_non_working_day.substring(0, 2));
			parsedMonth = map_month_prefix_to_zero_based_int(Local_non_working_day.substring(2, 5));



			if ((Day == parsedDay) && (Month == parsedMonth))
			{
				local_bool_res = true; 
			}
		}

		if (Local_non_working_day.length() == 7)
		{
			parsedDay = get_integer_value_day_of_month_from_string(Local_non_working_day.substring(0, 2));
			parsedMonth = map_month_prefix_to_zero_based_int(Local_non_working_day.substring(2, 5));
			parsedYear = get_integer_value_from_a_2_digit_number_in_a_string(Local_non_working_day.substring(5, 7));

			parsedYear = parsedYear + 2000;

			if ((Day == parsedDay) && (Month == parsedMonth) && (Year == parsedYear))
			{
				local_bool_res = true; 
			}
		}

		return local_bool_res;	
	}


	static int get_integer_value_day_of_month_from_string(String day_of_month_as_string)
	{
		int local_int_res;
		String local_temp_str, Local_day_of_month_as_string;
		local_int_res = -1; 
		local_temp_str= "";
		Local_day_of_month_as_string = "";

		local_temp_str = day_of_month_as_string.trim();

		if (local_temp_str.length() > 1)
		{
			Local_day_of_month_as_string = local_temp_str.substring(0, 2) ;

			Local_day_of_month_as_string = Local_day_of_month_as_string.trim();
		}

		try
		{
			local_int_res = Integer.parseInt(Local_day_of_month_as_string);
		}
		catch(Exception ex)
		{
			local_int_res = -2;
		}

		return local_int_res;	
	}


	static int map_month_prefix_to_zero_based_int(String month)
	{
		//{jan,feb,mar,apr,may,jun,jul,aug,sep,oct,nov,dec}

		int local_int;
		String local_str;
		local_int = 0; 
		local_str = "";

		local_str = month.trim();

		if (local_str.length() >= 3)
		{
			local_str = month.substring(0,3);
		}

		if (local_str.equalsIgnoreCase("jan") == true) 
		{
			local_int = 1 ;
		}

		if (local_str.equalsIgnoreCase("feb") == true) 		
		{
			local_int = 2 ;
		}

		if (local_str.equalsIgnoreCase("mar") == true) 
		{
			local_int = 3 ;
		}

		if (local_str.equalsIgnoreCase("apr") == true) 
		{
			local_int = 4 ;
		}

		if (local_str.equalsIgnoreCase("may") == true) 
		{
			local_int = 5 ;
		}


		if (local_str.equalsIgnoreCase("jun") == true) 
		{
			local_int = 6 ;
		}

		if (local_str.equalsIgnoreCase("jul") == true) 
		{
			local_int = 7 ;
		}

		if (local_str.equalsIgnoreCase("aug") == true) 
		{
			local_int = 8 ;
		}


		if (local_str.equalsIgnoreCase("sep") == true) 
		{
			local_int = 9 ;
		}

		if (local_str.equalsIgnoreCase("oct") == true)  
		{
			local_int = 10 ;
		}


		if (local_str.equalsIgnoreCase("nov") == true) 	 
		{
			local_int = 11 ;
		}

		if (local_str.equalsIgnoreCase("dec") == true)
		{
			local_int = 12 ;
		}

		local_int = local_int - 1;

		return local_int;
	}

	/// feriados
	//////////////////////////////
	/////////////////////////////

}
