# SULAM_SP_OUVIDORIA

Projeto de reformula��o da URA de ouvidorio	

## SETUP

Abaixo informa��es pertinentes para baixar, compilar, publicar e testar a aplica��o.

### Pr� Requisito
Vers�o do java, tomcat e aaod.

* JAVA: 8
* TOMCAT: 8
* AAOD: 7.2

### Fluxo B�sico
Para fluxo b�sico de baixar c�digo, alterar e subir altera��o executar 

* git clone https://bitbucket.org/sulamericaic/sulam_sp_ouvidoria.git
* git add -Av
* git commit -m "Mensagem"
* git push -u origin branch


### Baixando Projeto
Para baixar executar:
```
git clone https://bitbucket.org/sulamericaic/sulam_sp_ouvidoria.git
```

### Comandos com branch
Para criar uma nova branch a partir do c�digo de outra
```
git checkout -b branch
```

Para mudar de branch
```
git checkout branch
```

### Acrescentar c�digo
```
git add -Av
```

Para arquivo especifico
```
git add arquivo.extens�o
```

### Commit
```
git commit -m "Mensagem"
```

### Subindo altera��o
```
git push -u origin branch
```


